############ TESTING SCRIPT ################
import numpy as np
import pylab as pl
import scipy as sp
import scipy.sparse
from math import pi
import pdesolver as mypde

import matplotlib.pyplot as plt

# Initial Temperature Distribution Function
def u_I(x):
    y = np.sin(pi*x/L[-1])
    return y

# Exact Solution Function
def u_exact(x,t):
    y = np.exp(-kappa*(pi**2/L[-1]**2)*t)*np.sin(pi*x/L[-1])
    return y
	
def pNonHom(t):
    x = np.sin(2*pi*t)
    return x

def qNonHom(t):
    x = np.sin(2*pi*t)
    return x

###################################
	#Testing spacial dimension       #	
kappa = 1.0
T = np.linspace(0, 0.5, 101)
Errors = np.zeros((2,4))
for i in range(1,5):
	L = np.linspace(0, 1.0, 10**i+1)
	deltax = L[1] - L[0]
	print("DeltaX = ", deltax)
	#FESolutions = mypde.PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="HomDir")
	BESolutions = mypde.PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="HomDir")
	CNSolutions = mypde.PDESolve(L, T, u_I, kappa, method="CrankNicholson", bound_cond="HomDir")
	
	
	ExactSolutions = u_exact(L,T[-1])

	#FEerror = np.sqrt(np.sum((ExactSolutions-FESolutions)**2))
	BEerror = np.sqrt(np.sum((ExactSolutions-BESolutions)**2))
	CNerror = np.sqrt(np.sum((ExactSolutions-CNSolutions)**2))
	#Errors[0,i-1] = FEerror
	Errors[0,i-1] = BEerror
	Errors[1,i-1] = CNerror

plt.loglog([10,10**2,10**3,10**4],Errors[0,:])

plt.loglog([10,10**2,10**3,10**4],Errors[0,:],label = "BackEuler Error")

plt.loglog([10,10**2,10**3,10**4],Errors[1,:],label = "CrankNic Error")
plt.xlabel('Space dimension, Deltax', Fontsize=10)
plt.ylabel(', LSquared Norm Error at u(x,0.5)',Fontsize=10)
plt.legend(loc='upper right')
plt.show()



####################################
	#Testing time dimension       #	
# kappa = 1.0
# L = np.linspace(0, 1.0, 101)
# Errors = np.zeros((2,4))

# for i in range(1,5):
	# T = np.linspace(0, 1.0, 10**i+1)
	# deltat = T[1] - T[0]
	# print("DeltaT = ", deltat)
	#FESolutions = mypde.PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="HomDir")
	# BESolutions = mypde.PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="HomDir")
	# CNSolutions = mypde.PDESolve(L, T, u_I, kappa, method="CrankNicholson", bound_cond="HomDir")
	#FEerror = np.sqrt(np.sum((ExactSolutions-FESolutions)**2))
# BEerror = np.sqrt(np.sum((ExactSolutions-BESolutions)**2))
# CNerror = np.sqrt(np.sum((ExactSolutions-CNSolutions)**2))
	#Errors[0,i-1] = FEerror
	# Errors[0,i-1] = BEerror
	# Errors[1,i-1] = CNerror
	



#plt.loglog([10,10**2,10**3,10**4],Errors[0,:])

# plt.loglog([10,10**2,10**3,10**4],Errors[0,:],label = "BackEuler Error")

# plt.loglog([10,10**2,10**3,10**4],Errors[1,:],label = "CrankNic Error")
# plt.xlabel('Time dimension, Deltat', Fontsize=10)
# plt.ylabel(', u(x,0.5)',Fontsize=10)
# plt.legend(loc='upper right')
# plt.show()
