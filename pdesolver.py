import numpy as np
import pylab as pl
import scipy as sp
import scipy.sparse
from math import pi
import scipy.sparse.linalg as sparselinalg

def ForwardEuler(current_vals, mesh_fourier_no, BoundCondType, BoundVector=None, HeatSourceVector=None):
    new_vals = current_vals
    if BoundCondType == "HomDir":
        #create A_FE matrix
        A_FE = TriDiagMatrixMaker(current_vals.size-2,[mesh_fourier_no, 1-2*mesh_fourier_no, mesh_fourier_no])
        #create new values array and update with matrix calculation
        
        new_vals[1:-1] = A_FE.dot(current_vals[1:-1])
        if HeatSourceVector.all != None:
            new_vals[1:-1] += HeatSourceVector
            
    if BoundCondType == "NonHomDir":
        #create A_FE matrix
        A_FE = TriDiagMatrixMaker(current_vals.size-2,[mesh_fourier_no, 1-2*mesh_fourier_no, mesh_fourier_no])
        #create new values array and update with matrix calculation
        
        new_vals[1:-1] = A_FE.dot(current_vals[1:-1])+BoundVector*mesh_fourier_no
        if HeatSourceVector.all != None:
            new_vals[1:-1] += HeatSourceVector

    elif BoundCondType == "Neumann":
        #create A_FE matrix, full size from 0 to m!!
        A_FE = TriDiagMatrixMaker(current_vals.size,[mesh_fourier_no, 1-2*mesh_fourier_no, mesh_fourier_no])
        #edit for Neumann specific BCs
        A_FE[0,1] = 2*mesh_fourier_no
        A_FE[-1,-2] = 2*mesh_fourier_no

        if HeatSourceVector.all != None:
            new_vals = A_FE.dot(current_vals) + HeatSourceVector
        else:
            new_vals = A_FE.dot(current_vals)       

    return new_vals

def BackwardEuler(current_vals, mesh_fourier_no, BoundCondType, BoundVector=None, HeatSourceVector=None):
	
	new_vals = current_vals
	if BoundCondType == "HomDir":
		A_BE = TriDiagMatrixMaker(current_vals.size-2,[-1*mesh_fourier_no, 1+2*mesh_fourier_no, -1*mesh_fourier_no])
		if HeatSourceVector.all != None:
			current_vals[1:-1] += HeatSourceVector
		new_vals[1:-1] = sparselinalg.spsolve(A_BE, current_vals[1:-1])
	elif BoundCondType == "NonHomDir":
		A_BE = TriDiagMatrixMaker(current_vals.size-2,[-1*mesh_fourier_no, 1+2*mesh_fourier_no, -1*mesh_fourier_no])
		if HeatSourceVector.all != None:
			current_vals[1:-1] += HeatSourceVector
		new_vals[1:-1] = sparselinalg.spsolve(A_BE, current_vals[1:-1]+mesh_fourier_no*BoundVector)
	elif BoundCondType == "Neumann":
		A_BE = TriDiagMatrixMaker(current_vals.size,[-1*mesh_fourier_no, 1+2*mesh_fourier_no, -1*mesh_fourier_no])
		A_BE[0,1] = -2*mesh_fourier_no
		A_BE[-1,-2] = -2*mesh_fourier_no
		
		if HeatSourceVector.all != None:
			new_vals = sparselinalg.spsolve(A_BE, current_vals+BoundVector+HeatSourceVector)
		else:
			new_vals = sparselinalg.spsolve(A_BE, current_vals+BoundVector)       
		
	return new_vals

def CrankNicolson(current_vals, mesh_fourier_no, BoundCondType, BoundVector=None, HeatSourceVector=None):
    new_vals = current_vals
    if BoundCondType == "HomDir":
        #create A_CN and B_CN matrices
        A_CN = TriDiagMatrixMaker(current_vals.size-2, [-1*mesh_fourier_no/2, 1+mesh_fourier_no, -1*mesh_fourier_no/2])
        B_CN = TriDiagMatrixMaker(current_vals.size-2, [1*mesh_fourier_no/2, 1-mesh_fourier_no, 1*mesh_fourier_no/2])
        if HeatSourceVector.all != None:
            current_vals[1:-1] += HeatSourceVector
        alt = B_CN.dot(current_vals[1:-1])
        new_vals[1:-1] = sparselinalg.spsolve(A_CN, alt)
    elif BoundCondType == "NonHomDir":
        A_CN = TriDiagMatrixMaker(current_vals.size-2, [-1*mesh_fourier_no/2, 1+mesh_fourier_no, -1*mesh_fourier_no/2])
        B_CN = TriDiagMatrixMaker(current_vals.size-2, [1*mesh_fourier_no/2, 1-mesh_fourier_no, 1*mesh_fourier_no/2])
        if HeatSourceVector.all != None:
            current_vals[1:-1] += HeatSourceVector
        alt = B_CN.dot(current_vals[1:-1]+(mesh_fourier_no/2)*BoundVector)
        new_vals[1:-1] = sparselinalg.spsolve(A_CN, alt)
    elif BoundCondType == "Neumann":
        A_CN = TriDiagMatrixMaker(current_vals.size, [-1*mesh_fourier_no/2, 1+mesh_fourier_no, -1*mesh_fourier_no/2])
        B_CN = TriDiagMatrixMaker(current_vals.size, [1*mesh_fourier_no/2, 1-mesh_fourier_no, 1*mesh_fourier_no/2])
        A_CN[0,1] *= 2
        A_CN[-1,-2] *= 2
        B_CN[0,1] *= 2
        B_CN[-1,-2] *= 2

        if HeatSourceVector.all != None:
            new_vals = sparselinalg.spsolve(A_CN, B_CN.dot(current_vals)+BoundVector+HeatSourceVector)
        else:
            new_vals = sparselinalg.spsolve(A_CN, B_CN.dot(current_vals)+BoundVector)
        
    return new_vals

def TriDiagMatrixMaker(size, diagonal_vals):
    diag1 = np.zeros(size)
    diag2 = np.zeros(size)
    diag3 = np.zeros(size)
    for i in range(size):
        diag1[i] = diagonal_vals[0]
        diag2[i] = diagonal_vals[1]
        diag3[i] = diagonal_vals[2]
    diag_rows = np.array([diag1, diag2, diag3])
    positions = [-1,0,1]
    TriDiMatrix = sp.sparse.spdiags(diag_rows, positions, size, size, format='csr')
    
    return TriDiMatrix

def PDESolve(domain, trange, temp_distribution, diffusion_coefficient, method, bound_cond, Lbound_func=None, Rbound_func=None, HeatSource=None):
    #Gridpoint spacings
    deltat = trange[1] - trange[0]      #time steps
    deltax = domain[1] - domain[0]      #domain steps
    
    #Mesh Fourier number calculation
    lmbda = diffusion_coefficient * deltat / (deltax**2)     

    #Solution variables
    sols = np.zeros((trange.size, domain.size))      #Solution matrix
    u_j = np.zeros(domain.size)                      #Current time solutions
    u_jp1 = np.zeros(domain.size)                    #Step forward time solutions
    
    #initial solutions using temperature distr func
    for i in range(domain.size):
        u_j[i] = temp_distribution(domain[i])
    


    #Solve PDE for trange
    if method == "ForwardEuler":
        if bound_cond == "HomDir":
            #set initial solution at t=0
            sols[0,:] = u_j 
            u_j[0] = 0      #making sure boundaries
            u_j[-1] = 0     #are set to 0

            for i in range(1,trange.size):
                HeatSourceValues = np.zeros(domain.size-2)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain[1:-1],i)*deltat
                    
                u_jp1 = ForwardEuler(u_j, lmbda, bound_cond, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1

        elif bound_cond == "NonHomDir":
            #set up boundary condition arrays
            LeftBoundary = Lbound_func(trange)
            RightBoundary = Rbound_func(trange)
            
            #Set initial solution at t=0
            u_j[0] = LeftBoundary[0]
            u_j[-1] = RightBoundary[0]            
            sols[0,:] = u_j
            
            for i in range(1,trange.size):
                HeatSourceValues = np.zeros(domain.size-2)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain[1:-1],i)*deltat                
                #create boundary condition vector
                bound_vector = np.zeros(domain.size-2)
                bound_vector[0] = LeftBoundary[i]
                bound_vector[-1] = RightBoundary[i]
                #step forward in forward euler method
                u_jp1 = ForwardEuler(u_j, lmbda, bound_cond, bound_vector, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1


        elif bound_cond == "Neumann":
            #set up boundary condition arrays
            LeftBoundary = Lbound_func(domain[0],trange)
            RightBoundary = Rbound_func(domain[-1],trange)
            
            #set initial solution at t=0
            sols[0,:] = u_j

            for i in range(1, trange.size):
                HeatSourceValues = np.zeros(domain.size)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain,i)*deltat                
                bound_vector = np.zeros(domain.size)
                bound_vector[1] = -1*LeftBoundary[i-1]
                bound_vector[-2] = RightBoundary[i-1]

                u_jp1[:] = ForwardEuler(u_j[:], lmbda, bound_cond, HeatSourceVector=HeatSourceValues)
                u_jp1[:] = u_jp1[:] + 2*lmbda*deltax*bound_vector
                sols[i,:] = u_jp1[:]
                u_j[:] = u_jp1[:]
        else:
            print("Non-compatible boundary conditions argument.")

    elif method == "BackwardEuler":
        if bound_cond == "HomDir":
            #set initial solution at t=0
            sols[0,:] = u_j 
            u_j[0] = 0      #making sure boundaries
            u_j[-1] = 0     #are set to 0   

            for i in range(1,trange.size):
                HeatSourceValues = np.zeros(domain.size-2)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain[1:-1],i)*deltat                
                u_jp1 = BackwardEuler(u_j, lmbda, bound_cond, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1

        elif bound_cond == "NonHomDir":
            #set up boundary condition arrays
            LeftBoundary = Lbound_func(trange)
            RightBoundary = Rbound_func(trange)
            
            #Set initial solution at t=0 
            u_j[0] = LeftBoundary[0]
            u_j[-1] = RightBoundary[0]
            sols[0,:] = u_j
            
            BoundaryVector = np.zeros(u_j.size-2)
			
            for i in range(1,trange.size):
                HeatSourceValues = np.zeros(domain.size-2)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain[1:-1],i)*deltat               
                BoundaryVector[0] = LeftBoundary[i]
                BoundaryVector[-1] = RightBoundary[i]
                #step forward in forward euler method
                u_jp1 = BackwardEuler(u_j, lmbda, bound_cond, BoundaryVector, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1


        elif bound_cond == "Neumann":
            
            #set initial solution at t=0
            sols[0,:] = u_j
            BoundaryVector = np.zeros(u_j.size)
            for i in range(1, trange.size):
                HeatSourceValues = np.zeros(domain.size)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain,i)*deltat

                BoundaryVector[0] = Lbound_func(domain[0],i)*-1
                BoundaryVector[-1] = Rbound_func(domain[-1],i)
                BoundaryVector = BoundaryVector*2*lmbda*deltax
                u_jp1 = BackwardEuler(u_j, lmbda, bound_cond, BoundaryVector, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1

        


        else:
            print("Non-compatible boundary conditions argument.")
    
    elif method == "CrankNicolson":
        if bound_cond == "HomDir":
            #set initial solution at t=0
            sols[0,:] = u_j 
            u_j[0] = 0      #making sure boundaries
            u_j[-1] = 0     #are set to 0   

            for i in range(1,trange.size):
                HeatSourceValues = np.zeros(domain.size-2)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain[1:-1],i)*deltat                
                u_jp1 = CrankNicolson(u_j, lmbda, bound_cond, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1

        elif bound_cond == "NonHomDir":
            #set up boundary condition arrays
            LeftBoundary = Lbound_func(trange)
            RightBoundary = Rbound_func(trange)
            
            #Set initial solution at t=0
            u_j[0] = LeftBoundary[0]
            u_j[-1] = RightBoundary[0]            
            sols[0,:] = u_j
            
            BoundaryVector = np.zeros(u_j.size-2)
			
            for i in range(1,trange.size):
                HeatSourceValues = np.zeros(domain.size-2)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain[1:-1],i)*deltat 
                BoundaryVector[0] = LeftBoundary[i]+LeftBoundary[i-1]
                BoundaryVector[-1] = RightBoundary[i]+RightBoundary[i-1]
                #step forward in forward euler method
                u_jp1 = CrankNicolson(u_j, lmbda, bound_cond, BoundaryVector, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1


        elif bound_cond == "Neumann":
            
            #set initial solution at t=0
            sols[0,:] = u_j
            BoundaryVector = np.zeros(u_j.size)
            for i in range(1, trange.size):
                HeatSourceValues = np.zeros(domain.size)
                if HeatSource != None:
                    HeatSourceValues = HeatSource(domain,i)*deltat
                    
                BoundaryVector[0] = (Lbound_func(domain[0],i)+Lbound_func(domain[0],i-1))*-1
                BoundaryVector[-1] = Rbound_func(domain[-1],i)+Rbound_func(domain[-1],i-1)
                BoundaryVector = BoundaryVector*lmbda*deltax
                u_jp1 = CrankNicolson(u_j, lmbda, bound_cond, BoundaryVector, HeatSourceVector=HeatSourceValues)
                sols[i,:] = u_jp1
                u_j = u_jp1
        
        else:
            print("Non-compatible boundary conditions argument.")        
        
    return sols
	
	
def u_I(x):
    y = np.sin(pi*x/L[-1])
    return y

# Exact Solution Function
def u_exact(x,t):
    y = np.exp(-kappa*(pi**2/L[-1]**2)*t)*np.sin(pi*x/L[-1])
    return y
	
def pNonHom(t):
    x = np.sin(2*pi*t)
    return x

def qNonHom(t):
    x = np.sin(2*pi*t)
    return x

def pNeumann(x,t):
    y = t*0
    return y

def qNeumann(x,t):
	y = t*0
	return y
	
def HeatSourceFunc(x,t):
    y = 10*x*t
    return y
    
kappa = 1.0	
L = np.linspace(0, 1.0, 21)
T = np.linspace(0, 0.5, 5001)
#Solutions = PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="HomDir", HeatSource=HeatSourceFunc)
#Solutions = PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="NonHomDir", Lbound_func=pNonHom, Rbound_func=qNonHom, HeatSource=HeatSourceFunc)
#Solutions = PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="Neumann", Lbound_func=pNeumann, Rbound_func=qNeumann, HeatSource=HeatSourceFunc)

#Solutions = PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="HomDir", HeatSource=HeatSourceFunc)
#Solutions = PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="NonHomDir", Lbound_func=pNonHom, Rbound_func=qNonHom, HeatSource=HeatSourceFunc)
#Solutions = PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="Neumann", Lbound_func=pNeumann, Rbound_func=qNeumann, HeatSource=HeatSourceFunc)

#Solutions = PDESolve(L, T, u_I, kappa, method="CrankNicolson", bound_cond="HomDir", HeatSource=HeatSourceFunc)
#Solutions = PDESolve(L, T, u_I, kappa, method="CrankNicolson", bound_cond="NonHomDir", Lbound_func=pNonHom, Rbound_func=qNonHom)#, HeatSource=HeatSourceFunc)
#Solutions = PDESolve(L, T, u_I, kappa, method="CrankNicolson", bound_cond="Neumann", Lbound_func=pNeumann, Rbound_func=qNeumann, HeatSource=HeatSourceFunc)


pl.plot(L, Solutions[-1,:],'ro',label='num')
#xx = np.linspace(0,L[-1],250)
#pl.plot(xx,u_exact(xx,T[-1]),'b-',label='exact')
pl.xlabel('x')
pl.ylabel('u(x,0.5)')
pl.legend(loc='upper right')
pl.show()


