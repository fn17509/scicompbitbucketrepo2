import numpy as np
import pylab as pl
import scipy as sp
import scipy.sparse
from math import pi
import pdesolver as mypde

import matplotlib.pyplot as plt

# Initial Temperature Distribution
def u_I(x):
    y = np.sin(pi*x/L[-1])
    return y

# Exact Solution Function
def u_exact(x,t):
    y = np.exp(-kappa*(pi**2/L[-1]**2)*t)*np.sin(pi*x/L[-1])
    return y
	
# left side Non Homogenous Dirichlet boundary function
def pNonHom(t):
    x = np.sin(2*pi*t)
    return x

# right side Non Homogenous Diriclet boundary function
def qNonHom(t):
    x = np.sin(2*pi*t)
    return x

# left side Neumann boundary function
def pNeumann(x,t):
    y = t*0
    return y

# right side Neumann boundary function
def qNeumann(x,t):
	y = t*0
	return y
	
# Heat source function
def HeatSourceFunc(x,t):
    y = 3*x*t
    return y
    
kappa = 1.0	
L = np.linspace(0, 1.0, 21)
T = np.linspace(0, 0.5, 5001)

# Heat source included in all, remove heatsource argument if needed.

# FORWARD EULER FOR ALL BOUNDARY CONDITIONS
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="HomDir", HeatSource=HeatSourceFunc)
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="NonHomDir", Lbound_func=pNonHom, Rbound_func=qNonHom, HeatSource=HeatSourceFunc)
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="ForwardEuler", bound_cond="Neumann", Lbound_func=pNeumann, Rbound_func=qNeumann, HeatSource=HeatSourceFunc)

# BACKWARD EULER FOR ALL BOUNDARY CONDITIONS
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="HomDir", HeatSource=HeatSourceFunc)
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="NonHomDir", Lbound_func=pNonHom, Rbound_func=qNonHom, HeatSource=HeatSourceFunc)
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="BackwardEuler", bound_cond="Neumann", Lbound_func=pNeumann, Rbound_func=qNeumann, HeatSource=HeatSourceFunc)

# CRANK NICOLSON FOR ALL BOUNDARY CONDITIONS
Solutions = mypde.PDESolve(L, T, u_I, kappa, method="CrankNicolson", bound_cond="HomDir", HeatSource=HeatSourceFunc)
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="CrankNicolson", bound_cond="NonHomDir", Lbound_func=pNonHom, Rbound_func=qNonHom)#, HeatSource=HeatSourceFunc)
#Solutions = mypde.PDESolve(L, T, u_I, kappa, method="CrankNicolson", bound_cond="Neumann", Lbound_func=pNeumann, Rbound_func=qNeumann, HeatSource=HeatSourceFunc)


plt.plot(L, Solutions[-1,:],'ro',label='Numerical Solution')
#xx = np.linspace(0,L[-1],250)
#pl.plot(xx,u_exact(xx,T[-1]),'b-',label='exact')
plt.xlabel('Space dimension, x', Fontsize=10)
plt.ylabel('Temperature solutions at t = final, u(x,0.5)',Fontsize=10)
plt.legend(loc='upper right')
plt.show()